package model

import (
	"time"
)

/*
	解决仅更新非零值字段的方法有两种
	1. 将string 设置为 *string
	2. 使用sql的NULLxxx来解决
*/

type User struct {
	BaseModel
	Name     string  `gorm:"column:name; type:varchar(16); not null; default:''; comment:'名称'"`
	Age      int32   `gorm:"column:age; type:int; not null; default:0; comment:'年龄'"`
	Gender   int32   `gorm:"column:gender; type:int; not null; default:0; comment:'性别,1表示男,2表示女'"`
	Mobile   string  `gorm:"column:mobile; type:varchar(11); unique; not null; default:'00000000000'; index:idx_mobile; comment:'手机号'"`
	Email    *string `gorm:"column:email; type:varchar(32); unique; comment:'邮箱'"` // 创建用户时，如果string类型为零值，插入的是空字符串""，如果*string类型为零值，插入的是NULL
	Password string  `gorm:"column:password; type:varchar(128); not null; default:''; comment:'密码'"`
	// mysql的5.7以上版本不支持零日期格式（0000-0-0 00:00:00）, 所以需要设置指针类型
	// 如果不是指针类型，那么插入数据库的日期就是零日期，导致gorm插入默认数据出错。
	// 如果是指针类型，那么插入值为NULL
	// 此外，如果是指针类型，gorm使用结构体更新的时候不会忽略零值
	Birthday *time.Time `gorm:"column:birthday; type:datetime; comment:'出生日期'"`
	Role     int32      `gorm:"column:role; type:int; not null; default:1; comment:'角色,1表示非管理员,2表示管理员'"`
}

// 设置表名
func (User) TableName() string {
	return "users"
}
