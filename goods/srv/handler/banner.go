package handler

import (
	"context"

	"gitee.com/jason-laf/common/global"
	"gitee.com/jason-laf/common/pb/banner"
	"gitee.com/jason-laf/mxshop/goods/srv/model"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type BannerServer struct {
	banner.UnimplementedBannerServer
}

func (this *BannerServer) GetBannerList(ctx context.Context, req *banner.Empty) (*banner.BannerListResponse, error) {
	var banners []model.Banner
	res := global.GoodsDB.Find(&banners)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Error(codes.Internal, res.Error.Error())
	}

	var total int64
	global.GoodsDB.Model(&model.Banner{}).Count(&total)
	bannerListResp := banner.BannerListResponse{
		Total: int32(total),
		Data:  bannerModelToResp(banners),
	}
	return &bannerListResp, nil
}

func (this *BannerServer) CreateBanner(ctx context.Context, req *banner.CreateBannerRequest) (*banner.BannerInfoResponse, error) {
	// 1 查询轮播图是否存在
	var b model.Banner
	res := global.GoodsDB.Where(&model.Banner{Index: req.Index}).First(&b) // 当 First、Last、Take 方法找不到记录时，GORM 会返回 ErrRecordNotFound 错误
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected != 0 {
		return nil, status.Errorf(codes.AlreadyExists, "已存在")
	}

	// 2 创建轮播图
	b.Index = req.Index
	b.Image = req.Image
	b.Url = req.Url
	res = global.GoodsDB.Create(&b)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}

	createBannerResp := banner.BannerInfoResponse{
		Id:    b.Id,
		Index: b.Index,
		Image: b.Image,
		Url:   b.Url,
	}
	return &createBannerResp, nil
}

func (this *BannerServer) UpdateBanner(ctx context.Context, req *banner.CreateBannerRequest) (*banner.Empty, error) {
	// 1 查询轮播图是否存在
	var b model.Banner
	res := global.GoodsDB.Where(&model.Banner{BaseModel: model.BaseModel{Id: req.Id}}).First(&b) // 当 First、Last、Take 方法找不到记录时，GORM 会返回 ErrRecordNotFound 错误
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected != 0 {
		return nil, status.Errorf(codes.AlreadyExists, "用户已存在")
	}

	// 2 更新轮播图
	b.Index = req.Index
	b.Image = req.Image
	b.Url = req.Url
	res = global.GoodsDB.Updates(&b)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	return &banner.Empty{}, nil
}

func (this *BannerServer) DeleteBanner(ctx context.Context, req *banner.DeleteBannerRequest) (*banner.Empty, error) {
	res := global.GoodsDB.Delete(&model.Banner{}, req.Id)
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "品牌不存在")
	}
	return &banner.Empty{}, nil
}

func (this *BannerServer) Ping(ctx context.Context, req *banner.Empty) (*banner.Empty, error) {
	return &banner.Empty{}, nil
}
