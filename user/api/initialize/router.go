package initialize

import (
	"github.com/gin-gonic/gin"

	"gitee.com/jason-laf/common/middleware"
	"gitee.com/jason-laf/mxshop/user/api/api"
	"gitee.com/jason-laf/mxshop/user/api/global"
)

func Router() {
	// gin.SetMode(gin.ReleaseMode) // ReleaseMode 不会打印测试信息
	r := gin.Default()       // gin.Default() 与 gin.New() 的区别是前者使用了logger和recover中间件 logger会打印接口被访问后的日志， recover在接口panic后捕获异常
	r.Use(middleware.Cors()) // 配置跨域

	r.GET("/health", api.Check) // consul 健康检查
	apiRouter := r.Group("/v1")
	initRouter(apiRouter)
	global.Router = r
}

func initRouter(r *gin.RouterGroup) {
	userRouter := r.Group("/user")
	{
		// base
		userRouter.GET("/captcha", api.Captcha)   // 图片验证码
		userRouter.POST("/send_sms", api.SendSMS) // 短信验证码

		// user
		userRouter.POST("/register", api.Register)                                       // 注册
		userRouter.POST("/login", api.Login)                                             // 登陆
		userRouter.POST("/update", middleware.UserAuth(), api.Update)                    // 在这里使用Use，后面的路由也会使用该中间件
		userRouter.GET("/list", middleware.UserAuth(), middleware.AdminAuth(), api.List) // Todo: jwt+管理员权限
	}
}
