bind就是先创立一个结构体，然后把扔过来的参数通过一种绑定的形式直接映射到某一个结构体的实例上去

bind模式如何使用（一般用shouldbind）

bind模式一定要设置tag
bind可以绑json、form、uri
shouldbind

表单验证
即binding:“required”
自定义验证
即上面的那个限制18岁的传参
shouldbind

ShouldBindJSON使用方法
post一个json的body
ShouldBindJQuery使用方法
post http://127.0.0.1:8080/testBind?name=kaka&age=19&sex=false
ShouldBindUri使用方法
post http://127.0.0.1:8080/testBind/jack/18/true
ShouldBind使用方法
shouldbind会根据header里面的Content-Type来自动选择绑定器
————————————————
版权声明：本文为CSDN博主「kilmerfun」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/kilmerfun/article/details/123929945