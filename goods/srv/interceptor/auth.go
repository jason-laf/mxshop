package interceptor

import (
	"context"
	"errors"

	"google.golang.org/grpc/metadata"
)

// 鉴权逻辑
func Auth(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return errors.New("获取metadata失败")
	}
	if _, ok := md["appid"]; !ok {
		return errors.New("appId不存在")
	}
	if _, ok := md["appkey"]; !ok {
		return errors.New("appKey不存在")
	}
	appId := md["appid"][0]
	appKey := md["appkey"][0]
	if appId != "user-api-id" || appKey != "user-api-key" {
		return errors.New("无权限")
	}
	return nil
}
