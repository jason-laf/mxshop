package test

import "context"

type perRPCCredentials struct {
	AppId  string
	AppKey string
}

// GetRequestMetadata 获取当前请求认证所需的元数据（metadata）
func (p perRPCCredentials) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"appid":  p.AppId,
		"appkey": p.AppKey,
	}, nil
}

// RequireTransportSecurity 是否需要基于 TLS 认证进行安全传输
func (p perRPCCredentials) RequireTransportSecurity() bool {
	return false
}
