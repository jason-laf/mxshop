package interceptor

import (
	"go.uber.org/zap"
)

type Validator interface {
	Validate() error
}

// 验证器 - 验证参数是否合法
// 如果接口是内部使用的，其实可以不用加验证器，把验证放到api层去做
// req.UserReq 可以满足userReq的验证，所有的接口，但是还有其他的接口那怎么办
// 为什么要断言成接口，因为需要满足请求的结构体，所以不能写死一个类型的断言
// 所以断言成接口，接口里有valodate的方法就能使用了
func Validate(req interface{}) (err error) {
	if r, ok := req.(Validator); ok {
		if err := r.Validate(); err != nil {
			zap.S().Error("r.Validate() err: ", err)
			return err
		}
	}
	return nil
}
