package model

import (
	"context"
	"strconv"

	"gorm.io/gorm"

	commonDB "gitee.com/jason-laf/common/database"
	"gitee.com/jason-laf/mxshop/goods/srv/global"
)

type Category struct {
	BaseModel
	Name             string     `gorm:"type:varchar(20);not null;comment:'分类名'" json:"name"`       // 尽量设置为not null
	Level            int32      `gorm:"type:int;not null;default:1;comment:'默认级别为1'" json:"level"` // 由于proto中没有int类型，只有int32、int64，为了减少类型转换，所以这里设置为int32
	IsTab            bool       `gorm:"default:false;not null;comment:'是否是tab'" json:"istab"`
	ParentCategoryId int32      `json:"parentCategoryId"`
	ParentCategory   *Category  `json:"-"` // 使用短横线后，序列化时忽略该字段
	SubCategory      []Category `gorm:"foreignKey:ParentCategoryId;references:Id" json:"subCategory"`
	Url              string     `gorm:"type:varchar(500);default:url;not null;comment:'url'" json:"url"`
}

func (Category) TableName() string {
	return "category"
}

type Brand struct {
	BaseModel
	Name string `gorm:"type:varchar(50);not null;comment:'品牌名'"`
	Logo string `gorm:"type:varchar(200);not null;default:'';comment:'品牌logo的路径'"`
}

func (Brand) TableName() string {
	return "brand"
}

type Goods struct {
	BaseModel
	Category   Category
	CategoryId int32 `gorm:"type:int;not null;comment:'分类id'"`
	Brand      Brand
	BrandId    int32 `gorm:"type:int;not null;comment:'品牌id'"`

	// bool类型数据
	OnSale   bool `gorm:"default:false;not null;comment:'是否上架'"`
	ShipFree bool `gorm:"default:false;not null;comment:'是否包邮'"`
	IsNew    bool `gorm:"default:false;not null;comment:'是否新品'"`
	IsHot    bool `gorm:"default:false;not null;comment:'是否热销'"`

	// string类型数据
	Name       string `gorm:"type:varchar(500);not null;comment:'商品名'"`
	GoodsSn    string `gorm:"type:varchar(50);not null;default:'';comment:'商品编号'"`
	GoodsBrief string `gorm:"type:varchar(100);not null;comment:'商品简介'"`

	// 数值类型数据
	Stocks      int32   `gorm:"type:int;not null;default:0;comment:'库存量'"`
	ClickNum    int32   `gorm:"type:int;not null;default:0;comment:'点击量'"`
	FavNum      int32   `gorm:"type:int;not null;default:0;comment:'收藏量'"`
	SoldNum     int32   `gorm:"type:int;not null;default:0;comment:'销量'"`
	ShopPrice   float32 `gorm:"type:int;not null;default:0;comment:'商品价格'"`
	MarketPrice float32 `gorm:"type:float;not null;default:0;comment:'市场价'"` // float32会自动转换为数据库中的float类型，int类型会转换为bigint

	// 切片类型数据
	Images           commonDB.GormList `gorm:"type:varchar(2000);not null;comment:'商品图片'"`
	DescImages       commonDB.GormList `gorm:"type:varchar(2000);not null;comment:'商品详情图片'"`
	GoodsFrontImages string            `gorm:"type:varchar(200);not null;comment:'商品首页图片'"`
}

func (g *Goods) AfterCreate(tx *gorm.DB) error {
	esGoods := EsGoods{
		Id:          g.Id,
		CategoryId:  g.Category.Id,
		BrandId:     g.Brand.Id,
		OnSale:      g.OnSale,
		ShipFree:    g.ShipFree,
		IsNew:       g.IsNew,
		IsHot:       g.IsHot,
		Name:        g.Name,
		ClickNum:    g.ClickNum,
		SoldNum:     g.SoldNum,
		FavNum:      g.FavNum,
		MarketPrice: g.MarketPrice,
		GoodsBrief:  g.GoodsBrief,
		ShopPrice:   g.ShopPrice,
	}

	_, err := global.Es.Index().
		Index(esGoods.GetIndexName()).
		BodyJson(esGoods).Id(strconv.Itoa(int(g.Id))).
		Do(context.Background())
	if err != nil {
		return err
	}

	return nil
}

func (g *Goods) AfterUpdate(tx *gorm.DB) (err error) {
	esModel := EsGoods{
		Id:          g.Id,
		CategoryId:  g.CategoryId,
		BrandId:     g.BrandId,
		OnSale:      g.OnSale,
		ShipFree:    g.ShipFree,
		IsNew:       g.IsNew,
		IsHot:       g.IsHot,
		Name:        g.Name,
		ClickNum:    g.ClickNum,
		SoldNum:     g.SoldNum,
		FavNum:      g.FavNum,
		MarketPrice: g.MarketPrice,
		GoodsBrief:  g.GoodsBrief,
		ShopPrice:   g.ShopPrice,
	}

	_, err = global.Es.Update().Index(esModel.GetIndexName()).
		Doc(esModel).Id(strconv.Itoa(int(g.Id))).Do(context.Background())
	if err != nil {
		return err
	}
	return nil
}

func (g *Goods) AfterDelete(tx *gorm.DB) (err error) {
	_, err = global.Es.Delete().Index(EsGoods{}.GetIndexName()).Id(strconv.Itoa(int(g.Id))).Do(context.Background())
	if err != nil {
		return err
	}
	return nil
}

func (Goods) TableName() string {
	return "goods"
}

type GoodsCategoryBrand struct {
	BaseModel
	CategoryId int32 `gorm:"type:int;index:idx_category_brand,unique;comment:'分类id'"`
	Category   Category
	BrandId    int32 `gorm:"type:int;index:idx_category_brand,unique;comment:'品牌id'"`
	Brand      Brand
}

func (GoodsCategoryBrand) TableName() string {
	return "goods_category_brand"
}

type Banner struct {
	BaseModel
	Image string `gorm:"type:varchar(200);not null;comment:'图片路径'"`
	Url   string `gorm:"type:varchar(200);not null;comment:'跳转地址'"`
	Index int32  `gorm:"type:int;not null;default:1;comment:'排序'"`
}

func (Banner) TableName() string {
	return "banner"
}
