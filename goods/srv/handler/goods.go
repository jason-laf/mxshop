package handler

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitee.com/jason-laf/common/pb/goods"
)

type GoodsServer struct {
	goods.UnimplementedGoodsServer
}

func (this *GoodsServer) GetGoodsFilterList(context.Context, *goods.GetGoodsFilterListRequest) (*goods.GoodsListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetGoodsFilterList not implemented")
}
func (this *GoodsServer) GetBatchGoodsList(context.Context, *goods.GetBatchGoodsListRequest) (*goods.GoodsListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetBatchGoodsList not implemented")
}
func (this *GoodsServer) GetGoodsDetail(context.Context, *goods.GetGoodsDetailRequest) (*goods.GoodsInfoResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetGoodsDetail not implemented")
}
func (this *GoodsServer) CreateGoods(context.Context, *goods.CreateGoodsRequest) (*goods.GoodsInfoResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateGoods not implemented")
}
func (this *GoodsServer) UpdateGoods(context.Context, *goods.CreateGoodsRequest) (*goods.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateGoods not implemented")
}
func (this *GoodsServer) DeleteGoods(context.Context, *goods.DeleteGoodsRequest) (*goods.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteGoods not implemented")
}
func (this *GoodsServer) Ping(context.Context, *goods.Empty) (*goods.Empty, error) {
	return &goods.Empty{}, nil
}
