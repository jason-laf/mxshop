package main

import (
	"fmt"
	"net"

	"go.uber.org/zap"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"

	"gitee.com/jason-laf/common/database"
	"gitee.com/jason-laf/common/global"
	"gitee.com/jason-laf/common/grpc"
	"gitee.com/jason-laf/common/logger"
	"gitee.com/jason-laf/common/pb/user"
	"gitee.com/jason-laf/common/register"
	"gitee.com/jason-laf/common/util"
	"gitee.com/jason-laf/mxshop/user/srv/config"
	"gitee.com/jason-laf/mxshop/user/srv/handler"
)

func init() {
	logger.Init("debug")
	database.Init(config.GetConfig().Service.Name)
	grpc.Init()
}

func main() {
	// 1. 启动服务
	conf := config.GetConfig()
	port := conf.Service.Port
	// port, err := utils.GetFreePort()
	// if err != nil {
	// 	zap.S().Panicf(" utils.GetFreePort err: %s", err)
	// }
	zap.S().Infof("服务端口: %d", port)
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", "0.0.0.0", port))
	if err != nil {
		zap.S().Panicf("net.Listen err: %s", err)
		return
	}
	go func() {
		user.RegisterUserServer(global.GRPCServer, &handler.UserServer{})
		grpc_health_v1.RegisterHealthServer(global.GRPCServer, health.NewServer()) // 健康检查
		if err = global.GRPCServer.Serve(listener); err != nil {
			zap.S().Panicf("rpc服务启动失败: %s", err)
			return
		}
	}()

	// 2. 将服务注册到注册中心
	consul := register.NewClient(conf.Service.Name, conf.Service.Tags)
	if err := consul.Register(conf.Service.Check, port, "grpc"); err != nil {
		zap.S().Panicf("服务注册失败: %s", err)
		return
	}

	// 3. 优雅退出
	util.GracefulExit()

	// 4. 退出注册中心
	consul.Deregister()
}
