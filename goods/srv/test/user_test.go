package test

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"go.uber.org/zap"

	_ "github.com/mbobakov/grpc-consul-resolver" // It's important
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"

	"gitee.com/jason-laf/common/pb/user"
)

const (
	IP   = "192.168.1.19"
	PORT = 8500
	name = "user_srv" // rpc name
)

var (
	client user.UserClient
)

func init() {
	// 1、创建连接
	var err error
	var opts []grpc.DialOption
	interceptor := func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		start := time.Now()
		err := invoker(ctx, method, req, reply, cc, opts...) // invoker是真正调用业务逻辑的函数
		log.Printf("请求花费时间: %s", time.Since(start))
		return err
	}
	opts = append(opts, grpc.WithUnaryInterceptor(interceptor)) // 添加一元拦截器
	// opts = append(opts, grpc.WithPerRPCCredentials(perRPCCredentials{AppId: "user-api-id", AppKey: "user-api-key"})) // grpc鉴权
	// opts = append(opts, grpc.WithDefaultServiceConfig(`{"loadBalancingPolicy": "round_robin"}`))
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	// creds, _ := credentials.NewClientTLSFromFile("/home/jason/.ssh/tls/server.pem", "MXSHOP")
	// opts = append(opts, grpc.WithTransportCredentials(creds))

	conn, err := grpc.Dial(
		fmt.Sprintf("consul://%s:%d/%s?wait=14s", IP, PORT, name),
		// fmt.Sprintf("%s:%d", IP, PORT),
		opts...,
	)
	if err != nil {
		zap.S().Panicln(err)
	}

	// 2、调用
	client = user.NewUserClient(conn)
}

// 测试功能
func TestCreateUser(t *testing.T) {
	rsp, err := client.CreateUser(context.Background(), &user.CreateUserReq{
		Name:     "jason",
		Mobile:   "12321112226",
		Password: "123456",
	})
	if err != nil {
		t.Error(err)
	}
	t.Log(rsp)
}
func TestGetUserList(t *testing.T) {
	rsp, err := client.GetUserList(context.Background(), &user.GetUserListReq{})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(rsp)
}
func TestUpdateUser(t *testing.T) { // pass
	rsp, err := client.UpdateUser(context.Background(), &user.UpdateUserReq{
		Id:       2,
		Name:     "bob",
		Gender:   user.Gender_MALE,
		Mobile:   "12321112223",
		Email:    "alice1@gmail.com",
		Birthday: time.Now().Unix(),
	})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(rsp)
}
func TestGetUserByMobile(t *testing.T) {

	// ctx, _ := context.WithTimeout(context.Background(), time.Second*2) // 设置超时时间
	// defer cancel()
	rsp, err := client.GetUserByMobile(context.Background(), &user.GetUserByMobileReq{
		Mobile: "12321112223",
	})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(rsp)
	// if err != nil {
	// 	// 获取错误状态
	// 	statu, ok := status.FromError(err)
	// 	if ok {
	// 		// 判断是否为调用超时
	// 		// fmt.Println(statu.Code(), statu.Message())
	// 		if statu.Code() == codes.DeadlineExceeded {
	// 			log.Println(statu.Message())
	// 		}
	// 	}
	// }
	// fmt.Println(resp)
}
func TestGetUserById(t *testing.T) {
	rsp, err := client.GetUserById(context.Background(), &user.GetUserByIdReq{
		Id: 5,
	})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(rsp)
}

func TestListUser(t *testing.T) { // pass
	// metadata是可以加到header的数据，比如token（一般不会将token放入body）
	// 创建metadata的方式有两种：metadata.New()、metadata.Pairs()
	// 创建后再将md放入ctx中

	// 1. 创建md
	m := make(map[string]string)
	m["appName"] = "user-api"
	md := metadata.New(m) // New方法会将key转化为小写，所以在服务端取的时候要用小写取出来
	// 2. 放入ctx
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	rsp, err := client.GetUserList(ctx, &user.GetUserListReq{})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(rsp)
}

func TestCheckPasswordUser(t *testing.T) { // pass
	rsp, err := client.CheckPassword(context.Background(), &user.CheckPasswordReq{
		Password:          "123456",
		EncryptedPassword: "$pbkdf2-sha256$nP2irg2FGA4PVFfn$3270a87b5ecdecd3f881585ed8f43cb0ab21aa9920b48e2dc0782f0db32e7248",
	})
	if err != nil {
		t.Error(err)
	}
	fmt.Println(rsp)
}
