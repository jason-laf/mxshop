package handler

import (
	"gitee.com/jason-laf/common/pb/banner"
	"gitee.com/jason-laf/common/pb/brand"
	"gitee.com/jason-laf/mxshop/goods/srv/model"
)

func brandModelToResp(brands []model.Brand) []*brand.BrandInfoResponse {
	resp := make([]*brand.BrandInfoResponse, len(brands))
	for i, brandInfo := range brands {
		b := brand.BrandInfoResponse{
			Id:   brandInfo.Id,
			Name: brandInfo.Name,
			Logo: brandInfo.Logo,
		}
		resp[i] = &b
	}
	return resp
}

func bannerModelToResp(banners []model.Banner) []*banner.BannerInfoResponse {
	resp := make([]*banner.BannerInfoResponse, len(banners))
	for i, bannerInfo := range banners {
		b := banner.BannerInfoResponse{
			Id:    bannerInfo.Id,
			Index: bannerInfo.Index,
			Image: bannerInfo.Image,
			Url:   bannerInfo.Url,
		}
		resp[i] = &b
	}
	return resp
}

// // 模型转换
// func GoodsModelToResp(u *model.Goods) *Goods.GoodsModel {
// 	// 在grpc的message中字段有默认值，不能随便赋值nil进去，容易出错
// 	// 这里要搞清，哪些字段是有默认值
// 	// 因为数据库中的值为NULL，所以in.Email是nil
// 	var birthday int64
// 	var email string
// 	if u.Birthday != nil {
// 		birthday = u.Birthday.Unix()
// 	}
// 	if u.Email != nil {
// 		email = *u.Email
// 	}
// 	fmt.Println(u)
// 	return &Goods.GoodsModel{
// 		Id:          u.ID,
// 		CreatedAt:   u.CreatedAt.Unix(),
// 		UpdatedAt:   u.UpdatedAt.Unix(),
// 		DeletededAt: u.DeletedAt.Time.Unix(),
// 		Name:        u.Name,
// 		Gender:      Goods.Gender(u.Gender),
// 		Mobile:      u.Mobile,
// 		Email:       email,
// 		Password:    u.Password,
// 		Birthday:    birthday,
// 		Role:        u.Role,
// 	}
// }
// func GoodsModelsToRsp(us []model.Goods) []*Goods.GoodsModel {
// 	rsp := make([]*Goods.GoodsModel, len(us))
// 	for i, u := range us {
// 		rsp[i] = GoodsModelToResp(&u)
// 	}
// 	return rsp
// }
