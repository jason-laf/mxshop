package model

import (
	"fmt"
	"time"

	"gitee.com/jason-laf/common/pb/user"
)

type FormatTime time.Time

func (f FormatTime) MarshalJSON() ([]byte, error) {
	formatTime := fmt.Sprintf(`"%s"`, time.Time(f).Format("2006-01-02 15:04:05"))
	return []byte(formatTime), nil
}

type UserRsp struct {
	Name     string      `json:"name"`
	Age      int         `json:"age"`
	Gender   user.Gender `json:"gender"`
	Mobile   string      `json:"mobile"`
	Email    string      `json:"email"`
	Birthday FormatTime  `json:"birthday"`
}
