package api

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/mojocn/base64Captcha"
	"go.uber.org/zap"

	"gitee.com/jason-laf/common/global"
	"gitee.com/jason-laf/common/utils"
	"gitee.com/jason-laf/mxshop/user/api/form"
)

// Check 健康检查
func Check(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{ // 只要返回 200 就能检查通过
		"msg": "ok",
	})
}

// Captcha 获取验证码
func Captcha(c *gin.Context) {
	driver := base64Captcha.DefaultDriverDigit
	captcha := base64Captcha.NewCaptcha(driver, store)
	id, b64s, _, err := captcha.Generate()
	if err != nil {
		zap.S().Errorf("captcha.Generate() err: %s", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"msg": "生成验证码错误",
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"id":   id,
		"b64s": b64s,
	})
}

// SendSms 发送验证码
func SendSMS(c *gin.Context) {
	var SMS form.SMS
	if err := c.ShouldBind(&SMS); err != nil {
		utils.HandleValidatorError(c, err)
		return
	}
	code := utils.GenerateMessageCode(5)
	params := make(map[string]interface{}, 0)
	params["code"] = code

	// 发送验证码
	//client := sms.NewClient()
	//client.SetAppId(global.Config.SMS.AppId)
	//client.SetSecretKey(global.Config.SMS.SecretKey)
	//request := sms.NewRequest()
	//request.SetMethod("sms.message.send")
	//request.SetBizContent(sms.TemplateMessage{
	//	Mobile:     []string{SMS.Mobile},
	//	Type:       0,
	//	Sign:       "mxshop",
	//	TemplateId: "ST_2020101100000008",
	//	SendTime:   "",
	//	Params:     params,
	//})
	//if _, err := client.Execute(request); err != nil {
	//	zap.S().Errorf("client.Execute(request) err: %s", err)
	//	c.JSON(http.StatusInternalServerError, gin.H{
	//		"err": "发送验证码失败",
	//	})
	//	return
	//}

	// 设置缓存
	if err := global.RedisClient.Set(context.Background(), SMS.Mobile, code, 60*60*time.Second).Err(); err != nil {
		zap.S().Errorf("RedisClient.Set err: %s", err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"err": "设置验证码失败",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"msg": fmt.Sprintf("手机号 %s 发送验证码成功，验证码为：%s", SMS.Mobile, code),
	})
}
