package config

import (
	"fmt"
	"runtime"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

var (
	conf = &config{}
)

type config struct {
	Service service `mapstructure:"service" json:"service" yaml:"service"`
}

type service struct {
	Name  string   `mapstructure:"name"`
	Check string   `mapstructure:"check"`
	Tags  []string `mapstructure:"tags"`
	Port  int      `mapstructure:"port"`
}

func init() {
	v := viper.New()
	configPath := "./config_debug.yaml"
	if runtime.GOOS == "linux" {
		configPath = "./config_pro.yaml"
	}
	v.SetConfigFile(configPath)
	if err := v.ReadInConfig(); err != nil {
		zap.S().Panicf("配置文件错误: %s", err)
	}
	if err := v.Unmarshal(conf); err != nil {
		zap.S().Panicf("配置文件错误: %s", err)
	}
	fmt.Printf("配置信息: %+v\n", conf)

	// 动态监控配置文件变化
	v.WatchConfig()
	v.OnConfigChange(func(in fsnotify.Event) {
		if err := v.ReadInConfig(); err != nil {
			zap.S().Panicf("配置文件错误: %s", err)
		}
		if err := v.Unmarshal(conf); err != nil {
			zap.S().Panicf("配置文件错误: %s", err)
		}

		fmt.Printf("配置文件 %s 修改成功: %v\n", in.Name, conf)
	})
}

func GetConfig() *config {
	return conf
}
