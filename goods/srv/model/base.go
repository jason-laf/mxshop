package model

import (
	"time"

	"gorm.io/gorm"
)

type BaseModel struct {
	// 在数据库中，一个表中的外键如果与另一个表中的主键类型不一致（如int和bigint），那么创建表会出错
	// 两种解决方案，第一种方式是加tag（如type:int/bigint）；第二种方式是在struct中设置相同的类型
	Id        int32 `gorm:"primarykey; type:int"` // int32对应mysql中int，int64对应mysql中bigint
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
