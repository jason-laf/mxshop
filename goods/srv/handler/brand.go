package handler

import (
	"context"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"

	"gitee.com/jason-laf/common/global"
	"gitee.com/jason-laf/common/pb/brand"
	"gitee.com/jason-laf/common/util"
	"gitee.com/jason-laf/mxshop/goods/srv/model"
)

type BrandServer struct {
	brand.UnimplementedBrandServer
}

func (this *BrandServer) GetBrandFilterList(ctx context.Context, req *brand.GetBrandFilterListRequest) (*brand.BrandListResponse, error) {
	var brands []model.Brand
	res := global.GoodsDB.Scopes(util.Paginate(int(req.Pages), int(req.PagePerNums))).Find(&brands)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Error(codes.Internal, res.Error.Error())
	}

	var total int64
	global.GoodsDB.Model(&model.Brand{}).Count(&total)
	brandListResp := brand.BrandListResponse{
		Total: int32(total),
		Data:  brandModelToResp(brands),
	}
	return &brandListResp, nil
}

func (this *BrandServer) CreateBrand(ctx context.Context, req *brand.CreateBrandRequest) (*brand.BrandInfoResponse, error) {
	// 1 查询品牌是否存在
	var b model.Brand
	res := global.GoodsDB.Where(&model.Brand{Name: req.Name}).First(&b) // 当 First、Last、Take 方法找不到记录时，GORM 会返回 ErrRecordNotFound 错误
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected != 0 {
		return nil, status.Errorf(codes.AlreadyExists, "已存在")
	}

	// 2 创建品牌
	b.Name = req.Name
	b.Logo = req.Logo
	res = global.GoodsDB.Create(&b)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}

	createBrandResp := brand.BrandInfoResponse{
		Id:   b.Id,
		Name: b.Name,
		Logo: b.Logo,
	}
	return &createBrandResp, nil
}

func (this *BrandServer) UpdateBrand(ctx context.Context, req *brand.CreateBrandRequest) (*brand.Empty, error) {
	// 1 查询品牌是否存在
	var b model.Brand
	res := global.GoodsDB.Where(&model.Brand{BaseModel: model.BaseModel{Id: req.Id}}).First(&b) // 当 First、Last、Take 方法找不到记录时，GORM 会返回 ErrRecordNotFound 错误
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected != 0 {
		return nil, status.Errorf(codes.AlreadyExists, "用户已存在")
	}

	// 2 更新品牌
	b.Name = req.Name
	b.Logo = req.Logo
	res = global.GoodsDB.Updates(&b)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	return &brand.Empty{}, nil
}

func (this *BrandServer) DeleteBrand(ctx context.Context, req *brand.DeleteBrandRequest) (*brand.Empty, error) {
	res := global.GoodsDB.Delete(&model.Brand{}, req.Id)
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "品牌不存在")
	}
	return &brand.Empty{}, nil
}

func (this *BrandServer) Ping(ctx context.Context, req *brand.Empty) (*brand.Empty, error) {
	return &brand.Empty{}, nil
}
