package main

import (
	"fmt"

	"go.uber.org/zap"

	"gitee.com/jason-laf/common/grpc"
	"gitee.com/jason-laf/common/grpc/client"
	"gitee.com/jason-laf/common/logger"
	"gitee.com/jason-laf/common/redis"
	"gitee.com/jason-laf/common/register"
	"gitee.com/jason-laf/common/utils"
	"gitee.com/jason-laf/common/validator"
	"gitee.com/jason-laf/mxshop/user/api/config"
	"gitee.com/jason-laf/mxshop/user/api/global"
	"gitee.com/jason-laf/mxshop/user/api/initialize"
)

func init() {
	logger.Init("debug")
	redis.Init()
	validator.Init()
	grpc.Init()
	client.InitUserClient()
	initialize.Router()
}

func main() {
	// 1. 启动服务
	conf := config.GetConfig()
	port := conf.Service.Port
	zap.S().Infof("服务端口: %d", port)
	go func() {
		if err := global.Router.Run(fmt.Sprintf("%s:%d", "0.0.0.0", port)); err != nil {
			zap.S().Panicf("服务启动失败：%s", err)
		}
	}()

	// 2. 将服务注册到注册中心
	consul := register.NewClient(conf.Service.Name, conf.Service.Tags)
	if err := consul.Register(conf.Service.Check, port, "http"); err != nil {
		zap.S().Panicf("服务注册失败: %s", err)
	}

	// 3. 优雅退出
	utils.GracefulExit()

	// 4. 退出注册中心
	consul.Deregister()
}
