package handler

import (
	"fmt"

	"gitee.com/jason-laf/common/pb/user"
	"gitee.com/jason-laf/mxshop/user/srv/model"
)

// 模型转换
func UserModelToResp(u *model.User) *user.UserModel {
	// 在grpc的message中字段有默认值，不能随便赋值nil进去，容易出错
	// 这里要搞清，哪些字段是有默认值
	// 因为数据库中的值为NULL，所以in.Email是nil
	var birthday int64
	var email string
	if u.Birthday != nil {
		birthday = u.Birthday.Unix()
	}
	if u.Email != nil {
		email = *u.Email
	}
	fmt.Println(u)
	return &user.UserModel{
		Id:          u.ID,
		CreatedAt:   u.CreatedAt.Unix(),
		UpdatedAt:   u.UpdatedAt.Unix(),
		DeletededAt: u.DeletedAt.Time.Unix(),
		Name:        u.Name,
		Gender:      user.Gender(u.Gender),
		Mobile:      u.Mobile,
		Email:       email,
		Password:    u.Password,
		Birthday:    birthday,
		Role:        u.Role,
	}
}
func UserModelsToResp(us []model.User) []*user.UserModel {
	rsp := make([]*user.UserModel, len(us))
	for i, u := range us {
		rsp[i] = UserModelToResp(&u)
	}
	return rsp
}
