all: build-docker

rsa:
	ssh-keygen -t rsa -b 4096 -C jason.laf@foxmail.com

mysql:
	docker-compose -f docker-compose.yml up -d mysql

redis:
	docker-compose -f docker-compose.yml up -d redis

consul:
	docker-compose -f docker-compose.yml up -d consul

jenkins:
	docker-compose -f docker-compose.yml up -d jenkins

rabbitmq01:
	docker-compose -f docker-compose.yml up -d rabbitmq01
rabbitmq02:
	docker-compose -f docker-compose.yml up -d rabbitmq02
rabbitmq03:
	docker-compose -f docker-compose.yml up -d rabbitmq03

user_db:
	go run ./user/srv/model/main/main.go

run_user_srv:
	go run ./user/srv/main.go



# 生成dockerfile
# dockerfile:
# 	goctl docker --port 8888 -go main.go 

# build-docker:
# 	go mod vendor
# 	docker build -t grghub.grgchain.cn/dadao/app-center .

# # 使用xorm根据数据库生成模型
# genxorm-model:
# 	genxorm -d dadao_appcenter -host=10.252.79.31  -port=33306  -u=root -p=123456  -dir=./models

# # 生成rpc文件
# goctl-rpc:
# 	goctl rpc protoc ./rpc/proto/app.proto --go_out=./rpc --go-grpc_out=./rpc --zrpc_out=./rpc
# 	goctl rpc protoc ./rpc/proto/applimit.proto --go_out=./rpc --go-grpc_out=./rpc --zrpc_out=./rpc

# # 生成rest文件
# goctl-rest:
# 	goctl api go -api ./rest/api/appcenter.api -dir ./rest
# 	goctl api plugin -plugin goctl-swagger="swagger" -api ./rest/api/appcenter.api -dir ./rest/doc
# 	goctl api doc --dir ./rest/api/appcenter.api --o ./rest/doc/appcenter.md

# docker-image:
# 	docker run --name app-center -p 8888:8888 -p 8889:8889 -d grghub.grgchain.cn/dadao/app-center
