package form

type Page struct {
	Pn    uint32 `json:"pn" form:"pn"`
	PSize uint32 `json:"psize" form:"psize"`
}

type Login struct {
	Mobile    string `json:"mobile" form:"mobile" binding:"required,mobile"` // binding中多个条件中间不能加空格
	Password  string `json:"password" form:"password" binding:"required,min=6,max=20"`
	Captcha   string `json:"captcha" form:"captcha" binding:"required,min=5,max=5"`
	CaptchaId string `json:"captcha_id" form:"captcha_id" binding:"required"`
}

type Update struct {
	Name       string `json:"name" form:"name" binding:"max=20"`
	Age        int32  `json:"age" form:"age" binding:"gte=0,lte=130"`
	Gender     int    `json:"gender" form:"gender" binding:"oneof=1 2"` // 必填
	Email      string `json:"email" form:"email" binding:"email"`       // 必填
	Birthday   int64  `json:"birthday" form:"birthday" binding:"gte=0"`
	Password   string `json:"password" form:"password" binding:"min=6,max=20"`           // 必填
	RePassword string `json:"re_password" form:"re_password" binding:"eqfield=Password"` // 跨字段验证
}

type Register struct {
	Mobile     string `json:"mobile" form:"mobile" binding:"required,mobile"`
	Password   string `json:"password" form:"password" binding:"required,min=6,max=20"`
	RePassword string `json:"re_password" form:"re_password" binding:"eqfield=Password"`
	Code       string `json:"code" form:"code " binding:"required,min=5,max=5"`
}

type SMS struct {
	Mobile string `json:"mobile" form:"mobile" binding:"required,mobile"`
	Type   int    `json:"type" form:"type" binding:"required,oneof=1 2"` // 1表示注册，2表示登陆
}
