package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"gitee.com/jason-laf/common/utils"
)

type SendSMSBody struct {
	Mobile string `json:"mobile"`
	Type   int    `json:"type"`
}
type SendSMSRespBody struct {
	Message string `json:"msg"`
}

type RegisterBody struct {
	Mobile     string `json:"mobile"`
	Password   string `json:"password"`
	RePassword string `json:"re_password"`
	Code       string `json:"code"`
}

const (
	UserSRVBaseURL = "http://192.168.1.19:8080"
	Concurrency    = 70
	SleepTime      = 1
)

func main() {
	for {
		for i := 0; i < Concurrency; i++ {
			go Do()
		}
		time.Sleep(SleepTime * time.Second)
	}
}

func Do() {
	mobile := "15" + utils.GenerateMessageCode(9)
	messageCode, err := getMessageCode(mobile)
	if err != nil {
		log.Printf("getMessageCode err: %s", err)
		return
	}
	if err = register(mobile, utils.GenerateMessageCode(8), messageCode); err != nil {
		log.Printf("register err: %s", err)
	} else {
		log.Printf("register sucess, mobile: %s", mobile)
	}
}

func getMessageCode(mobile string) (string, error) {
	headers := map[string]string{
		"Content-Type": "application/json",
	}
	sendSMSBody := SendSMSBody{
		Mobile: mobile,
		Type:   1,
	}
	var sendSMSRespBody SendSMSRespBody
	resp, err := utils.NewRestyClient(context.TODO(), headers, utils.Marshal(&sendSMSBody)).SetResult(&sendSMSRespBody).Post(fmt.Sprintf("%s/v1/user/send_sms", UserSRVBaseURL))
	if err != nil {
		log.Printf("POST err: %s", err)
		return "", err
	}
	if !resp.IsSuccess() {
		log.Printf("POST failed, statusCode: %d", resp.StatusCode())
		return "", errors.New("POST failed")
	}
	return strings.Split(sendSMSRespBody.Message, "：")[1], nil
}

func register(mobile, password, code string) error {
	headers := map[string]string{
		"Content-Type": "application/json",
	}
	registerBody := RegisterBody{
		Mobile:     mobile,
		Password:   password,
		RePassword: password,
		Code:       code,
	}
	resp, err := utils.NewRestyClient(context.TODO(), headers, utils.Marshal(&registerBody)).Post(fmt.Sprintf("%s/v1/user/register", UserSRVBaseURL))
	if err != nil {
		log.Printf("POST err: %s", err)
		return err
	}
	if !resp.IsSuccess() {
		log.Printf("POST failed, statusCode: %d", resp.StatusCode())
		return errors.New("POST failed")
	}

	return nil
}
