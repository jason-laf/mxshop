package handler

import (
	"context"
	"crypto/sha256"
	"fmt"
	"strings"
	"time"

	"github.com/anaskhan96/go-password-encoder"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"

	"gitee.com/jason-laf/common/global"
	"gitee.com/jason-laf/common/pb/user"
	"gitee.com/jason-laf/common/util"
	"gitee.com/jason-laf/mxshop/user/srv/model"
)

type UserServer struct {
	user.UnimplementedUserServer
}

var options = password.Options{ // MD5加密和验证的Options
	SaltLen:      16,
	Iterations:   100,
	KeyLen:       32,
	HashFunction: sha256.New,
}

func (this *UserServer) CreateUser(c context.Context, req *user.CreateUserReq) (*user.CreateUserResp, error) {
	var u model.User

	// 查询用户是否存在
	res := global.UserDB.Where(&model.User{Mobile: req.Mobile}).First(&u) // 当 First、Last、Take 方法找不到记录时，GORM 会返回 ErrRecordNotFound 错误
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected != 0 {
		return nil, status.Errorf(codes.AlreadyExists, "用户已存在")
	}

	// 盐值加密
	salt, encodedPwd := password.Encode(req.Password, &options)
	u = model.User{
		Name:     req.Mobile,
		Mobile:   req.Name,
		Password: fmt.Sprintf("$pbkdf2-sha256$%s$%s", salt, encodedPwd),
	}

	// 创建用户
	if res = global.UserDB.Create(&u); res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}

	return &user.CreateUserResp{
		User: UserModelToResp(&u),
	}, nil
}

func (this *UserServer) UpdateUser(c context.Context, req *user.UpdateUserReq) (*user.Empty, error) {
	var u model.User
	var pwd string
	var email *string
	var birth *time.Time
	// 更新用户之前需要先查询用户是否存在
	res := global.UserDB.First(&u, req.Id) // 通过主键查询
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "用户不存在")
	}

	if req.Mobile != "" {
		res := global.UserDB.Where(model.User{Mobile: req.Mobile}).First(&model.User{})
		// 如果没查找到会有err，不能直接返回，因为是正常的业务逻辑
		if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
			zap.S().Errorln(res.Error)
			return nil, status.Errorf(codes.Internal, res.Error.Error())
		}
		if res.RowsAffected != 0 {
			return nil, status.Errorf(codes.AlreadyExists, "该手机号已存在")
		}
	}

	// 指针类型
	if req.Email != "" {
		email = &req.Email
	} else {
		email = nil
	}
	if req.Birthday != 0 {
		b := time.Unix(req.Birthday, 0) // 将int类型的时间转为Time类型
		birth = &b
	} else {
		birth = nil
	}

	if req.Password != "" {
		salt, encodedPwd := password.Encode(req.Password, &options)
		pwd = fmt.Sprintf("$pbkdf2-sha256$%s$%s", salt, encodedPwd)
	}

	if res := global.UserDB.Model(model.User{BaseModel: model.BaseModel{ID: req.Id}}).Updates(model.User{ // 使用struct更新时只会更新非零字段
		Name:     req.Name,
		Age:      req.Age,
		Gender:   int32(req.Gender), // 如果Gender = 0，那么gorm会忽略这个字段
		Mobile:   req.Mobile,
		Email:    email,
		Password: pwd,
		Birthday: birth,
	}); res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	return &user.Empty{}, nil
}

func (this *UserServer) GetUserByMobile(c context.Context, req *user.GetUserByMobileReq) (*user.GetUserByMobileResp, error) {
	var u model.User
	res := global.UserDB.Where(model.User{Mobile: req.Mobile}).First(&u)
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "用户不存在")
	}
	return &user.GetUserByMobileResp{
		User: UserModelToResp(&u),
	}, nil
}

func (this *UserServer) GetUserById(c context.Context, req *user.GetUserByIdReq) (*user.GetUserByIdResp, error) {
	var u model.User
	res := global.UserDB.First(&u, req.Id)
	if res.Error != nil && res.Error != gorm.ErrRecordNotFound {
		zap.S().Errorln(res.Error)
		return nil, status.Errorf(codes.Internal, res.Error.Error())
	}
	if res.RowsAffected == 0 {
		return nil, status.Errorf(codes.NotFound, "用户不存在")
	}
	return &user.GetUserByIdResp{
		User: UserModelToResp(&u),
	}, nil
}

func (this *UserServer) GetUserList(c context.Context, req *user.GetUserListReq) (*user.GetUserListResp, error) {
	var us []model.User
	res := global.UserDB.Scopes(util.Paginate(int(req.Pn), int(req.PSize))).Find(&us)
	if res.Error != nil {
		zap.S().Errorln(res.Error)
		return nil, status.Error(codes.Internal, res.Error.Error())
	}

	var total int64
	global.UserDB.Model(&model.User{}).Count(&total)
	userListResp := user.GetUserListResp{
		Total: int32(total),
		Users: UserModelsToResp(us),
	}

	return &userListResp, nil
}

func (this *UserServer) CheckPassword(c context.Context, req *user.CheckPasswordReq) (*user.CheckPasswordResp, error) {
	pwds := strings.Split(req.EncryptedPassword, "$")
	return &user.CheckPasswordResp{
		IsSuccess: password.Verify(req.Password, pwds[2], pwds[3], &options),
	}, nil
}

func (this *UserServer) Ping(c context.Context, req *user.Empty) (*user.Empty, error) {
	return &user.Empty{}, nil
}
